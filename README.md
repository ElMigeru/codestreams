# CodeStreams. Es mi código personalizado para AWS Lambda 

Hola, Si estás viendo esto es porque te he compartido mis implementaciones para alimentar Data Warehouse y Data Lakes por medio de Eventos con AWS Lambda o Bien para campañas masivas de Email, SMS y hacer tracking de esos eventos con SNS.

# Folder: Lambda All Trigger Events

Capturar gran parte de los eventos en AWS, incluyendo monitoreo (Memoria RAM y Disk Usage; Para poder capturar este evento es necesario realizar un cronjob con el script en perl de monitoreo) de instancias, procesos como webapps (Latencia), Logs de ELB incluyendo health checks y request context, Cognito y API Gateway.
El storage puede realizarse a DynamoDB o bien a S3 como objetos json.

# Folder: Storage SNS-SES Events

Una lambda simple para hacer tracking de correos para campañas masivas de Email. 

# Folder: Campañas Masivas de Correo con SES

Esta lambda depende de la creación del topic para hacer trigger a los eventos de Email. El topic lo adjuntas en el Header del correo a enviar.

# Folder: SEND SMS

Esta lambda es para realizar campañas masivas con la Plataforma de contactalos.com

# Folder: Lambda to Redshift

Esta lambda es un ejemplo para recibir data según un modelo y enviarlo a Aws Redshift mediante la implementación Serverless Framework Commands