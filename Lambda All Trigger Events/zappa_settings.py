# Generated by Zappa
APP_MODULE='run'
APP_FUNCTION='app'
EXCEPTION_HANDLER=None
DEBUG=True
LOG_LEVEL='DEBUG'
BINARY_SUPPORT=True
CONTEXT_HEADER_MAPPINGS={}
DOMAIN=None
BASE_PATH=None
ENVIRONMENT_VARIABLES={'AWS_REGION': 'us-east-1'}
API_STAGE='dev'
PROJECT_NAME='genial-io-ml'
SETTINGS_FILE=None
DJANGO_SETTINGS=None
ARCHIVE_PATH='s3://genialio/dev_genial-io-ml_current_project.tar.gz'
SLIM_HANDLER=True
AWS_EVENT_MAPPING={}
AWS_BOT_EVENT_MAPPING={}
COGNITO_TRIGGER_MAPPING={}
ASYNC_RESPONSE_TABLE=''
