## Pre-requisites

La configuración de AWS Redshift no está en el ejemplo y es necesario para volcar los datos desde nuestro trabajo ETL. Configuralo y guarda el endpoint del cluster.

### Redshift cluster endpoint

`<cluster_name>.xxxxxxxxxxxx.<region>.redshift.amazonaws.com:<port>`

### DB Connection String

`postgres://<username>:<password>@<hostname>:<port>/<db_name>`
where `<hostname>:<port>` is the cluster endpoint

## Install dependencies

`$ pip install -r requirements.txt`

## Install plugins

`$ sls plugin install -n serverless-python-requirements`

This will install the plugin and will add the `plugins` section in the `serverless.yml` file.

## Run locally

`sls invoke local -f etlSample`

## Run

`sls invoke -f etlSample`
