var aws = require('aws-sdk');
const https = require('https');
var querystring = require("querystring");
const moment = require('moment-timezone');
var ses = new aws.SES();
var s3 = new aws.S3();

const mongoose = require('mongoose');
const uri = 'mongodb://pi:27017/listas';

var cdate = moment().tz('America/Bogota');

let conn = null;

const smsSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    body_message: String,
    to: String,
    send_date: Date,
    status: Number
});

const SMS = mongoose.model('sms_queue', smsSchema);

exports.handler = function (event, context, callback) {
    function sendAndUpdate(sms) {
        let msg = querystring.escape(sms.body_message);
        console.log(msg, "msg encoded")
        let url_envio = `https://contactalos.com/services/rs/sendsms.php?user=youruser&password=yourpassword&destination=${sms.to}&message=${msg}`
        
        https.get(url_envio, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                console.log("end request", JSON.parse(data).explanation);
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });

        sms.status = 1;
        sms.save();
        console.log('SMS sent successfully');
    }
    
    context.callbackWaitsForEmptyEventLoop = false;
    
    if (conn == null) {
        mongoose.connect(uri, {
             "auth": { "authSource": "admin" },
             "user": "root",
             "pass": "y3vQou7amqie",
             "useNewUrlParser": true
         });
        conn = mongoose.connection;
        conn.on('error', function() {
            console.log("Error");
        });
        conn.once('open', function() {
         // we're connected!
            console.log("we're connected!");
        });
    }
    
    console.log(cdate, "cdate")
    
    const q = SMS.find({status: 0, send_date:{"$lte": cdate}}).limit(100);
    q.exec(function(err, results) {
        if(!err) {
            for(var i = 0; i < results.length; i++) {
                var sms = results[i];                
                // send email
                sendAndUpdate(sms);
            }
        } else {
            console.log(err);
        }
    });
};
