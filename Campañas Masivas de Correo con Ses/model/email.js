var mongoose = require('mongoose');

const emailSchema = new mongoose.Schema({
    subject: String,
    body_message: String,
    to_name: String,
    to: String,
    from_name: String,
    from: String,
    attachments: [String],
    send_date: Date,
    status: Number
});

const Email = mongoose.model('Email', emailSchema);

export default Email;