var mongoose = require('mongoose');

const connectDb = () => {
  return mongoose.connect('mongodb://34.201.217.223:27017/listas');
};

export default connectDb;