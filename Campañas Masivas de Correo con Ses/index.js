var aws = require('aws-sdk');
var nodemailer = require('nodemailer');
const mongoose = require('mongoose');
const uri = 'mongodb://ip:27017/listas';
const moment = require('moment-timezone');

let conn = null;
var cdate = moment().tz('America/Bogota');

var ses = new aws.SES();
var s3 = new aws.S3();

var transporter = nodemailer.createTransport({
    SES: ses
});

const emailSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    subject: String,
    body_message: String,
    to_name: String,
    to: String,
    from_name: String,
    from: String,
    attachments: [String],
    send_date: Date,
    status: Number,
    campaing_id: Number,
    ses_message_id: String
}, {
    strict: false
  });

const Email = mongoose.model('email_queue', emailSchema);

exports.handler = function (event, context, callback) {

	function sendAndUpdate(email, mailOptions) {
        transporter.sendMail(mailOptions, function (err, info) {
            message_id = ''
            if (err) {
                console.log(err);
                console.log('Error sending email');
                email.status = 3;
            } else {
                console.log('Email sent successfully');
                email.status = 1;
                message_id = info.response;
            }
            email.ses_message_id = message_id
            email.save();
            //email.update({_id: mongoose.Types.ObjectId(email._id)},{$set: {ses_message_id: message_id}});
        });
    }

    context.callbackWaitsForEmptyEventLoop = false;

    if (conn == null) {
        mongoose.connect(uri, {
             "auth": { "authSource": "admin" },
             "user": "root",
             "pass": "password",
             "useNewUrlParser": true
         });
        conn = mongoose.connection;
        conn.on('error', function() {
            console.log("Error");
        });
        conn.once('open', function() {
         // we're connected!
            console.log("we're connected!");
        });
    }

    console.log(cdate, "cdate")

    const q = Email.find({status: 0, send_date:{"$lte": cdate}}).limit(100);
    q.exec(function(err, results) {
        if(!err) {
            for(var i = 0; i < results.length; i++) {
                var email = results[i];
                //Este código es para soportar Emojis en el asunto
                //var msg = email.subject;
                //var r = decodeURIComponent(msg.toString().replace(/%/g,'Ç'));
                //var msg = r.replace(/Ç/g, '%'); 
                var mailOptions = {
	                from: email.from,
	                subject: email.subject,
	                html: email.body_message,
	                to: email.to,
                    attachments: [],
                    headers: {'X-SES-CONFIGURATION-SET':'topic'
                }
	            };               
                // send email
                sendAndUpdate(email, mailOptions);
            }
        } else {
            console.log(err);
        }
    });
};
